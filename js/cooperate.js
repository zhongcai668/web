$(function(){

    /*判断今日开奖的彩种样式设置*/
    (function(){
        var IsType = { Ssq:"2,4,0",
            Dlt:"1,3,6",
            Qlc:"1,3,5",
            Qxc:"2,5,0"
        };
        for(var i in IsType){
            if($(".ulMode1:eq(1) a[link='"+i+"']"))
                if(IsType[i].indexOf(new Date().getDay())>-1)
                    $(".ulMode1:eq(1) a[link='"+i+"']").append("<span class='iconKjToday'></span>");
        }
    })();
    /*合买查询条件事件绑定*/
    $("#findstr").keyup(function(){ do_search();});
    $("#tcSelect,#ztSelect,#jeSelect,#mfSelect,#jdSelect,#bdSelect,#IsLotid,#myqihao").change(function(){do_search();});

    /*返回继续购买*/

    /*合买导航栏样式及彩种选择事件绑定*/
    $(".ulMode1 li,.leftLink").click(function(){
            try{$(".ulMode1 li[class='active']").removeClass();
                $(this).addClass("active");}catch(e){;}
            do_search();
        }
    )
});

function Record_(record)
{
    var am = record,ar = Math.floor(am / 1000000),val_="";
    if (ar > 0 )
    {
        val_ += "<img src='/images/y4.gif' />";
        val_ += "<img src='/images/s" + ar + ".png' />";
        am = am - 1000000 * ar
    }
    ar = Math.floor(am / 100000)
    if (ar > 0) {
        val_ += "<img src='/images/y3.gif' />";
        val_ += "<img src='/images/s" + ar + ".png' />";
        am = am - 100000 * ar
    }
    ar = Math.floor(am / 10000)
    if (ar > 0) {
        val_ += "<img src='/images/y2.gif' />";
        val_ += "<img src='/images/s" + ar + ".png' />";
        am = am - 10000 * ar
    }
    ar = Math.floor(am / 1000)
    if (ar > 0) {
        val_ += "<img src='/images/y1.gif' />";
        val_ += "<img src='/images/s" + ar + ".png' />";
        am = am - 1000 * ar
    }
    if(val_ == "") {
        val_ += "<img src='/images/y1.gif' />";
        val_ += "<img src='/images/s" + 1 + ".png' />";
    }
    return val_;
}

function DomAdd(data)
{

    var is_mobile = data.is_mobile;
    var tbody = $("table.rec_table > tbody");
    var  page = {};
    if(data.length<0 || data =="[]" || data ==""){
        tbody.html("<tr><td colspan='9'>抱歉！没有找到符合条件的结果！</td></tr>");
        page.pagesize = page.pageindex = page.countrs =0;
        createpage(page);return;
    }
    var context = data.list,page_ = data.page,tr = "";
    $.each(context,function(key,value){
        var lottName = '';
        var key_1 = (page_.pageindex>1?(Number(page_.pageindex)-1)*20+Number(key):Number(key))+1 ;

        value.url = '/index/cooperateDetail/'+value.id;
        if (value.available<=0) {
            val = "<td><b class='red'>成功</b></td><td>"
        }else if (value.is_over) {
            val = "<td><b class='c66'>已截止</b></td><td>"
        }else{
            val = "<td><input type='text' class='rec_text' value='1' style='width: 38px;' name='buynum' />"
                +"<input type='hidden' name='pid' value='"+value.id+"' />"
                +"<input type='hidden' name='senumber' value='"+value.available+"' />"
                +"<input type='hidden' name='onemoney' value='"+value.onemoney+"' /></td>"
                +"<td><a class='btn_cy' title='' href='javascript:;' onclick='AddProject(this)'>购买</a>";
        }
        var finish_rate = ((value.all - value.available) / value.all * 100).toFixed(2);
        tr += '<tr class="'+((key_1%2)==0?"th_on":"th_even")+'">'
            +  (is_mobile?'':'<td>'+key_1+'</td>')
            +'<td><a href="'+value.url+'">'+ getLotteryName(value.lotteryname, is_mobile)+'</td>'
            +'<td>'+value.username +'</td>'
            + (is_mobile?'':'<td>'+ Record_(value.record)+'</td>')
            +'<td class="tr">'+value.money+'&nbsp;</td>'
            + '<td class="tl">'+value.available+'份</td>';
         tr+= is_mobile?'':'<td style="text-align:left;font-family: simsun">'+ finish_rate +'%' + '<span style="color:#fff;background:#F60;margin-left:40px ">保</span>' + '<br><div style="width:98px;height:4px; border:1px solid #cccccc;"><img style="margin-left:-1px;margin-top:-16px;display: inline" src="/images/bar_space.gif" width="'+ finish_rate +'" height="6"></div></td>';
         tr+= val + (is_mobile && value.available > 0 && !value.is_over? '' : '<a href="'+value.url+'" class="btn_xx">详情</a></td></tr>');
    });
    tbody.html(tr);
    page.pagesize = Number(page_.pagesize);
    page.pageindex = Number(page_.pageindex);
    page.countpage = Number(page_.countpage);
    page.countrs = Number(page_.countrs);
    createpage(page);
}


function getLotteryName(name, is_mobile) {
    if(!is_mobile) {
        return name;
    }
    var len = name.length;
    if(len < 4) {
        return name;
    }
    return name.substring(0, 2) + '..' + name.substring(len -1);
}

function do_search(page_no){
    $.ajax({
        type: "GET",
        url: "/Game/getHmList",
        data: {
            page_no:page_no
        },
        dataType:'json',
        success: function(data){
            DomAdd(eval(data));
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}
//购买按钮事件
function AddProject(obj)
{
    var td = $(obj).parent().prev(),buynum = $(td).find("input[name=buynum]").val(),
        pid = $(td).find("input[name=pid]").val(),
        senumber = $(td).find("input[name=senumber]").val(),
        onemoney = $(td).find("input[name=onemoney]").val();
    if (buynum=="")
    {
        winjinAlert("认购份数不能为空！","alert");
        $(td).find("buynum").focus();
        return;
    }
    else if (Number(buynum) <= 0)
    {
        winjinAlert("认购份数不能为空！","alert");
        $(td).find("buynum").focus();
        return;
    }
    else if (Number(buynum) > Number(senumber))
    {

        winjinAlert("您认购份数不能大于剩余份数！","alert");
        $("#buynum").focus();
        return;
    }
    var msg = "<p style='text-align:left;text-indent:2em;'>您好，请您确认</p>";
    msg = msg + "<p style='text-align:left;text-indent:2em;'>认购份数：<font color='red' style='font-weight:bold'>"+buynum+"</font>份</p>";
    msg = msg + "<p style='text-align:left;text-indent:2em;'>认购金额：<font color='red' style='font-weight:bold'>￥"+onemoney+"元</font></p>";
    var width = $(window).width() > 600?450:280;



    $('#wanjinDialog').html(msg).dialog({
        title:'投注提示',
        resizable: false,
        width:width,
        minHeight:220,
        modal: true,
        buttons: {
            "确定购买": function() {
                $( this ).dialog( "close");
                $.ajax('/game/buyHm', {
                    data:{
                        bet_id:pid,
                        buy_volume:buynum,
                        one_money:onemoney
                    },
                    type:'post',
                    dataType:'json',
                    error:function(xhr, textStatus, errorThrown){
                        winjinAlert("交易失败,请重新提交！","alert");
                    },
                    success:function(data, textStatus, xhr){
                        if('恭喜您购买成功' == data) {
                            winjinAlert('恭喜您购买成功',"ok");
                        } else {
                            winjinAlert(data,"alert");
                        }
                        do_search($('#page-index').text());
                    },
                    complete:function(xhr, textStatus){
                    }
                });
            },
            "取消购买": function() {
                $( this ).dialog( "close" );
                return false;
            }
        }
    });

   // alertdiv.confirm(msg,function(){ add_project(buynum,pid,onemoney)});

}



//参加合买
function add_project(buynum,pid,onemoney){
    $.ajax({
        type: "POST",
        url: "/game/buyHm",
        data: {
            bet_id:pid,
            buy_volume:buynum,
            one_money:onemoney
        },
        dataType:'json',
        success: function(data){
            console.log(data);
                if('恭喜您购买成功' == data) {
                    winjinAlert('恭喜您购买成功',"ok");
                } else {
                    winjinAlert('恭喜您购买成功',"alert");
                }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            winjinAlert("交易失败,请重新提交！","alert");
        },
        complete: function (jqXHR, textStatus) {
        }
    });
}

function createpage(page){
    $("#page_wrapper").empty();
    if(page.countrs >0)
    {
        var str = "";

        if(page.pageindex>1)
        {
            str +="<a class='h_l' href='javascript:;' onclick='setpageindex(1)'>首页</a>"
            str +="<a class='pre'  href='javascript:;' onclick='setpageindex("+(page.pageindex-1)+")'>上一页</a>"
        }
        else
        {
            str +="<a class='h_l' href='javascript:;'>首页</a>"
            str +="<a class='pre' href='javascript:;'>上一页</a>"
        }
        if(page.pageindex<page.countpage)
        {
            str +="<a class='next'  href='javascript:;' onclick='setpageindex("+(page.pageindex+1)+")'>下一页</a>"

        }
        else
        {
            str +="<a class='next'  href='javascript:;'>下一页</a>"

        }
        str += "页次：";
        str += "<span style='color:red' id='page-index'>"+page.pageindex+"</span>"
        str += "/";
        str += page.countpage
        str += "共："+page.countrs+"条 "
        $("#page_wrapper").html(str)
    }
}


function setpageindex(index)
{
    do_search(index)
}