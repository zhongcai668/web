<!DOCTYPE html>
<html>
<head lang="en">
<?php $this->display('inc_base.php',0) ?>
<style type="text/css">
.sale{}
.saleBox{ width:1000px; margin:20px auto; height:100%}
.saleBox br{ clear:both}
.saleBox .genre{background:#efefef; height:308px; margin-bottom:23px}
.saleBox .genre .l{ width:515px; }
.saleBox .genre .r{ width:415px; padding:35px 20px 0 50px; }
.saleBox .genre .r h3{ color:#a50909; font-size:22px; font-family:"微软雅黑"; font-weight: normal;}
.saleBox .genre .r p{ line-height:24px; margin-top:15px; color:#9b9b9b; }
.saleBox .genre .r .saleBut{ height:37px; margin-top:16px; margin-bottom:13px} 
.saleBox .genre .r .saleBut .on,.saleBox .genre .r .saleBut .off{line-height:37px;width:127px; border-radius:2px; display:inline-block; text-align:center;}
.saleBox .genre .r .saleBut .l{ margin-right:15px}
.saleBox .genre .r .saleBut .off{background:#979797; color:#e1e1e1;}
.saleBox .genre .r .saleBut .on{ background:#a20b2a; color:#fff}
.saleBox .genre .r .info span{ color:#a20b2a; font-size:18px; font-family:Arial }
.saleBox .genre .r .orange{ margin-top:20px}
.l{ float:left}
.r{ float:right}
.orange{ color:#ff7206}
</style>
</head>
<body>
    
<?php $this->display('inc_header.php') ?>
<div class="saleBox">
<!-- 开始-->
  <div class="genre" style="display:none">
     <div class="l"><img src="/images/sale/5.jpg" width="515" height="308"/></div>
     <div class="r"><h3>感恩六月，新注册即送彩金18元！</h3>
       <p>感恩六月，感恩有你，新注册并绑定收款方式的会员即可申请18元彩金，只需10倍流水即可提款！彩金丰厚十分好拿，亲还在等什么！<br /> </p>
       <div class="orange">【活动时间】：2015年07月02日至2015年12月02日</div>
       <div class="orange">【活动编号】：A32 | *该活动不可与其它活动重叠使用!</div>
     </div>
  </div>
  <div class="genre">
     <div class="l"><img src="/images/sale/4.jpg" width="515" height="308"/></div>
     <div class="r"><h3>平台三周年，首存50送18！</h3>
       <p><?= $this->settings['webName'] ?>好运奖金不断，存送优惠不歇！已注册未存款用户，首次在<?= $this->settings['webName'] ?>最低充值50元即可获得18元彩金！联系客服申请！<br /> </p>
       <div class="orange">【活动时间】：2015年07月02日至2015年12月02日</div>
       <div class="orange">【活动编号】：A35 | *该活动不可与其它活动重叠使用!</div>
     </div>
  </div>
  <div class="genre">
     <div class="l"><img src="/images/sale/2.jpg" width="515" height="308"/></div>
     <div class="r"><h3>每天签到天天送！</h3>
       <p>周年狂欢，每天签到天天送！！活动期间每天签到都可获得系统赠送1元现金，无须流水无须手续！<br /> </p>
       <div class="orange">【活动时间】：2015年07月02日至2015年12月02日</div>
       <div class="orange">【活动编号】：A36 | *该活动为系统活动,无限制、可重叠使用!</div>
     </div>
  </div>
  <div class="genre">
     <div class="l"><img src="/images/sale/1.jpg" width="515" height="308"/></div>
     <div class="r"><h3>欢乐红包周末随时送</h3>
       <p>即日起只要在当周内在铂金有存款记录并于当周六晚20：00至21:00之间向客服申请，即可获得8元彩金。活动每周都有，只限每周六20:00至21:00申请哦！<br /> </p>
       <div class="orange">【活动时间】：2015年07月02日至2015年12月02日</div>
       <div class="orange">【活动编号】：A38 | *该活动无限制、可重叠使用!</div>
     </div>
  </div>
<!-- 完-->
</div>

  <?php $this->display('inc_footer.php') ?>
</body>
</html>
