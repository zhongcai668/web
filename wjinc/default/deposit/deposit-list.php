        <!--下注列表-->
 <table width="100%" class='table_b'>
        <thead>
            <thead>
            <tr class="table_b_th">
                <td>编号</td>
                <td>收益金额</td>
                <td>收益时长(天)</td>
                <td>成功时间</td>
                <td>备注</td>
            </tr>
            </thead>
            <tbody class="table_b_tr">
            <?php
                $sql="select * from {$this->prename}deposit_log where uid={$this->user['uid']} ";
				if($_GET['fromTime'] && $_GET['endTime']){
                    $fromTime=strtotime($_GET['fromTime']);
                    $endTime=strtotime($_GET['endTime']);
                    $sql.=" and actionTime between $fromTime and $endTime";
                }elseif($_GET['fromTime']){
                    $sql.=' and actionTime>='.strtotime($_GET['fromTime']);
                }elseif($_GET['endTime']){
                    $sql.=' and actionTime<'.(strtotime($_GET['endTime']));
                }else{
					
					if($GLOBALS['fromTime'] && $GLOBALS['toTime']) $sql.=' and actionTime between '.$GLOBALS['fromTime'].' and '.$GLOBALS['toTime'].' ';
				}
                $sql.=' order by actionTime desc';
                
                $pageSize=15;
                
                $list=$this->getPage($sql, $this->page, $pageSize);
                if($list['data']) foreach($list['data'] as $var){
            ?>
            <tr>
                <td><?=$var['id']?></td>
                <td><?=$this->iff($var['depositCoin']>0, $var['depositCoin'], '--')?></td>
                <td><?=$var['CoinTime']?></td>
                <td><?=date('Y-m-d H:i:s', $var['actionTime'])?></td>
                <td><?=$var['info']?></td>
            </tr>
            <?php }else{ ?>
            <tr>
                <td colspan="7" align="center">没有余额宝收益记录</td>
            </tr>
            <?php } ?>
            </tbody>
            
        </table>
        <?php
            $this->display('inc_page.php', 0, $list['total'], $this->pageSize, "/index.php/deposit/main-{page}?fromTime={$_GET['fromTime']}&endTime={$_GET['endTime']}");
        ?>
