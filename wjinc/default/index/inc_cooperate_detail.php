<!DOCTYPE html>
<html>
<head lang="en">
    <?php $this->display('inc_base.php',0, $this->settings['webName'] . '祝您财运享通') ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" href="/skin/css/order.css" />
    <link rel="stylesheet" href="/skin/css/detail.css" />
    <script language="javascript" type="text/javascript" src="/js/cooperate.js"></script>
</head>

<body>
<?php $this->display('inc_header.php') ?>

<div id="docBody">
    <div class="header clearfix">
        <div class="menu">
            <div class="docBody clearfix">

                <div id="mainBody"> <div class="game_header clearfix">
                        <div class="headerBox">
                            <div class="det_t_bg">
                                <div class="s-logo Ssc-logo"></div>
                                <div class="clearfix titleBox">
                                    <h1>重庆时时彩</h1>
                                    <div class=abstract>
                                        <span>发起时间：<?= date('Y-m-d H:i:s', $this->order['actionTime'])?></span>
                                        <span>编号：<?= $this->order['wjorderId']?></span>
                                        <strong class=gameperiod>期号：<?= $this->order['actionNo']?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="topWrap clearfix">
                        <div class="userInfoBox">
                            <div class="clearfix userInfo">
                                <img style="display:inline;float:left;margin:0 15px 0 10px;border:1px solid #C2C2C2;height:60px;width:60px;"
                                     src="<?= $this->order['user_info']['avatar_url']?>" />
                                <div class="userName">
                                    发起人<strong id="username"><?= $this->order['user_info']['username']?></strong>
                                </div>
                            </div>
                            <ul class="list clearfix">
                                <li><span class="textL" style="width: 70px">个人战绩：</span><em  title="<?=$this->order['user_info']['scoreTotal']?>" id="user-level"></em>
                                </li>
                            </ul>

                        </div>

                        <div class="scheme">
                            <ul class="list">
                                <li class="caseStatus" style="font-weight:900">方案进度：<div class="progressBox baodi" style="margin-left: 10px">
                                        <div class="progressBar"><span style="width:<?= $finish_rate = round(($this->order['total_volume'] - $this->order['available_volume'])/$this->order['total_volume'] * 100, 2)?>%" class="progress"><strong><?=$finish_rate?></strong>%</span><i style="right:50.00%"></i></div></div></li>

                            </ul>
                            <ul id="centerh" class="ulTable clearfix">
                                <li><span>总金额</span><strong><?= $this->order['total_amount']?>元</strong></li>
                                <li><span>剩余份数</span><strong><?= $this->order['available_volume']?> 份</strong></li>
                                <li><span>保底金额</span><strong><?= $this->order['hmEnable'] ?></strong></li>
                                <li><span>总份数</span><strong><?= $this->order['total_volume']?> 份</strong></li>
                                <li><span>每份金额</span><strong><?= round($this->order['total_amount'] / $this->order['total_volume'], 2)?> 元</strong></li>
                                <li style="border-right:0;"><span>中奖金额</span><strong id="winmoney"><font color="<?= $this->order['bonus'] > 0 ?'#FF0000' : '#228B22'?>"><?= $this->order['status'] == 1 ? $this->order['bonus'] : '未开奖'?></font> </strong></li>
                            </ul>
                        </div>
                        <div id="tzot" style="padding:10px">
                            <table cellspacing='0' cellpadding='0' width='100%' border='0' id='gaopinNumberTable' class='user_table' style='height:100px'>
                                <thead>
                                <tr><td>投注号码</td></tr>
                                <?php if($this->order['public_level']){ ?>
                                <tr><td style="background:#FFFFFF; padding:10px; text-align:center;"><?= strtr($this->order['actionData'], [' ' => ','])?></td></tr>
                                <?php } else { ?>
                                <tr><td style="background:#FFFFFF; padding:10px; text-align:center;">该方案选择永久保密（仅方案发起人可见）</td></tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="paybox" class="paybox">
                            <?php if($this->order['status']){ ?>
                                <div id="jiezhi">
                                    <a class="end_Btn" href="javascript:void (0);" rel="nofollow" title="方案已截止"></a>
                                    <a  href="/index/cooperateHall">您可以选择参加其他合买>></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="number_user_wrap">
                        <!--选号详情 start -->
                        <div id="show_list_div">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="user_table hmxq">
                                <thead>
                                <tr>
                                    <td width="10%">期号</td>
                                    <td width="10%">金额</td>
                                    <td width="10%">倍数</td>
                                    <td width="10%">开奖号码</td>
                                    <td width="15%">开奖时间</td>
                                    <td width="10%">奖金</td>
                                    <td width="10%">状态</td>

                                </tr>
                                <tbody>
                                <tr>
                                    <td><?= $this->order['actionNo']?></td>
                                    <td>￥<?= $this->order['total_amount']?></td>
                                    <td>
                                        <?= $this->order['beiShu']?>

                                    </td>
                                    <td><?= $this->order['lotteryNo']?></td>
                                    <td><?= date('Y/m/d H:i:s', $this->order['kjTime'])?></td>
                                    <td><?= $this->order['bonus']?></td>
                                    <td><?php if($this->order['status'] == 1){ ?>
                                            <span style="color: <?= $this->order['bonus'] > 0 ?'#FF0000' : '#228B22'?>"><?= $this->order['bonus'] > 0 ? '已中奖' : '未中奖'?></span>
                                        <?php } else { ?>
                                            <span style="color: #666">未开奖</span>
                                        <?php } ?></td>

                                </tr>
                                </tbody>

                                </thead>
                            </table>
                            <!--选号详情 start -->
                            <ul class="number_user_tab clearfix" >
                                <li class="an_cur"><a href="javascript:void(0);">参与用户</a></li>
                            </ul>
                            <!--合买参与用户 start -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="user_table" >
                                <thead>
                                <tr>
                                    <th width="15%">序号</th>
                                    <th width="20%">用户名</th>
                                    <th width="20%">认购金额（元）</th>
                                    <th width="20%">中奖金额（元）</th>
                                    <th width="25%">参与时间</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $sql="select * from {$this->prename}hm where bet_id={$this->order['id']} AND status=1 order by id";
                                $groups=$this->getRows($sql);

                                foreach($groups as $k => $group){

                                    ?>
                                    <tr>
                                        <td><?= $k + 1?></td>
                                        <td><?= $this->getName($group['nickname'])?></td>
                                        <td>￥<?= $group['buy_amount']?></td>
                                        <td>￥<?= $group['bonus_amount']?></td>
                                        <td><?= $group['created_at']?></td>
                                    </tr>
                                <?php } ?>


                                </tbody>

                            </table>
                            <!--合买参与用户 end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</div>

<?php $this->display('inc_footer.php') ?>

<script>
    var  user_level = $("#user-level");
    user_level.html(Record_(user_level.attr('title')));

</script>

</body>
</html>













