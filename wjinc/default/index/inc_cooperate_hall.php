<!DOCTYPE html>
<html>
<head lang="en">
    <?php $this->display('inc_base.php',0, $this->settings['webName'] . '祝您财运享通') ?>
    <script language="javascript" type="text/javascript" src="/js/alertdiv.js"></script>
    <script language="javascript" type="text/javascript" src="/js/cooperate.js"></script>
    <link href="/skin/css/cooperate.css" type="text/css" rel="stylesheet" />
    <style>
        #tuijian li .buyNow {
            display: block;
            margin: 5px auto 10px auto;
            height: 37px;
            width: 126px;
            background: url(/images/buyBtn.png) -94px 0 no-repeat;
        }
    </style>
</head>
<?php $this->display('inc_header.php') ?>

<div id="docBody">

    <!--头部导航结束-->
    <div class="sider_area">
        <div class="all_title"><a href="/index/cooperateHall" class="titLink">合买大厅首页</a></div>
        <!-- 彩种列表 -->
        <div class="block_con">
            <h3>竞技彩</h3>
            <ul class="ulMode1">
            </ul>
            <h3>高频彩</h3>
            <ul class="ulMode1">
                <li><a href="/index/game/1/74">重庆时时彩</a></li>
                <li><a href="/index/game/6/80">广东11选5</a></li>
            </ul>
            <h3>数字彩</h3>
            <ul class="ulMode1">
                <li><a href="/index/game/9/12">福彩3D<span class="iconKjToday"></span></a></li>
                <li><a href="/index/game/10/16">排列三<span class="iconKjToday"></span></a></li>
            </ul>

        </div>
        <!-- 帮助模块 -->
        <div class="modBody">
            <h3>合买帮助</h3>
            <ul>
                <li><a href="/index3" target="_blank">什么是自动跟单？</a></li>

                <li><a href="/index3" target="_blank">什么叫方案保底？</a></li>

                <li><a href="/index3" target="_blank">合买中奖后的奖金怎么分配？</a></li>

                <li><a href="/index3" target="_blank">如何使合买方案尽快满员？</a></li>

                <li><a href="/index3" target="_blank">如何参与方案合买？</a></li>
            </ul>
        </div>
        <!-- 广告位 -->
    </div>
    <div id="bd" style="width:900px; margin-top:7px;margin-left: 205px;min-height: 800px">
        <div class="hmzx-top">
            <select id="myqihao">
                <option value="" selected="selected">最近三天合买</option>
                <option value="1">今天全部合买</option>
                <option value="7">最近七天合买</option>
                <option value="30">最近一月合买</option>
                <option value="90">最近三月合买</option>
            </select>
            <select id="jeSelect" name="jeSelect">
                <option value=",">方案金额</option>
                <option value="0,10">10元以下</option>
                <option value="10,100">10-100元</option>
                <option value="100,1000">100-1千元</option>
                <option value="1000,10000">1千-1万元</option>
                <option value="10000,30000">1万-3万元</option>
            </select>
            <select id="jdSelect" name="jdSelect">
                <option value="">进度</option>
                <option value="0">10及以下</option>
                <option value="1">10%-20%</option>
                <option value="2">20%-30%</option>
                <option value="3">30%-40%</option>
                <option value="4">40%-50%</option>
                <option value="5">50%-60%</option>
                <option value="6">60%-70%</option>
                <option value="7">70%-80%</option>
                <option value="8">80%-90%</option>
                <option value="9">90%以上</option>
            </select>
            <input type="hidden" id="findstr">
        </div>
        <div class="hm-plan" >
            <div class="content">
                <div class="c-wrap">
                    <div class="c-inner">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0" id="list_data" class="rec_table">
                            <colgroup>
                                <col width="5%">
                                <col width="12%">
                                <col width="12%">
                                <col width="10%">
                                <col width="11%">
                                <col width="10%">
                                <col width="10%">
                                <col width="16%">
                                <col width="14%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th>排序</th>
                                <th>彩种</th>
                                <th>发起人</th>
                                <th>战绩</th>
                                <th class="tr">方案金额&nbsp;|&nbsp;</th>
                                <th class="tl">剩余份数</th>
                                <th>进度</th>
                                <th>认购份数</th>
                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div id="page_wrapper" class="page"></div>

                    </div>
                </div>
                <b class="c-tl"></b>
                <b class="c-tr"></b>
            </div>
        </div>
    </div>
</div>

<script>
    $.getJSON('/Game/getHmList', function(data){
        DomAdd(eval(data));
    })
</script>
<!--网站底部开始-->

<!--弹出投注提示窗口开始-->
<div class="notifyicon tip-3" style="width: 360px; overflow: hidden; top: 295px; left: -9999px;">
    <div class="notifyicon_space"></div>
    <div class="notifyicon_arrow"><s></s><em></em></div>
    <div class="notifyicon_content"></div>
</div>
<div id="_box_common" style="display: none;">
    <div style="padding:10px">
        <br/>
        <font style="font-size: 14px;line-height:30px; color:#666;" class="t_weight"></font>
    </div>
</div>
<div id="_config_common" style="display: none;">
    <div class="pop_layer_content">
        <div class="pop_add">
            <br/>
            <font style="font-size: 14px;line-height:30px; color:#666;"class="t_weight"></font>
        </div>
    </div>
</div>

</div>



<?php $this->display('inc_footer.php') ?>
</body>
</html>













