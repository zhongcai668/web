<?php $this->display('inc_left.php') ?>
<div class="pagemain">
    <div class="search">
        时间：<input type="text" name="fromTime" class="datainput"  value="<?=date('Y-m-d', strtotime('-1 year'))?>"/>至<input type="text" name="toTime"  class="datainput" value="<?=date('Y-m-d')?>"/>
    </div>
    <div class="display biao-cont">
        <!--下注列表-->
        <table width="100%" class='table_b'>
        <thead>
            <thead>
            <tr class="table_b_th">
                <td>编号</td>
                <td>充值金额</td>
                <td>实际到账</td>
              <!--  <td>充值银行</td>-->
                <td>状态</td>
                <td>成功时间</td>
                <td>备注</td>
            </tr>
            </thead>
            <tbody class="table_b_tr">
            <?php
                $sql="select a.* from {$this->prename}member_recharge a where a.uid={$this->user['uid']} and a.isDelete=0";

                $sql.=' order by a.id desc';
                
                $pageSize=10;
                
                $list=$this->getPage($sql, $this->page, $pageSize);
                if($list['data']) foreach($list['data'] as $var){
            ?>
            <tr>
                <td><?=$var['id']?></td>
                <td><?=$var['amount']?></td>
                <td><?=$this->iff($var['rechargeAmount']>0, $var['rechargeAmount'], '--')?></td>
        <!--        <td><?/*=$this->iff($var['bankName'], $var['bankName'], '--')*/?></td>-->
                <td><?=$this->iff($var['state'], '充值成功', '<span style="color:#653809">正在处理</span>')?></td>
                <td><?=$this->iff($var['state'], date('Y-m-d H:i:s', $var['actionTime']), '--')?></td>
                <td><?='系统充值'?></td>
            </tr>
            <?php }else{ ?>
            <tr>
                <td colspan="7" align="center">没有充值记录</td>
            </tr>
            <?php } ?>
            </tbody>
            
        </table>
        <?php
            $this->display('inc_page.php', 0, $list['total'], 10, "/index.php/cash/rechargeLog-{page}");
        ?>
        <!--下注列表 end -->
    </div>
	
</div>
<!--以下为模板代码-->
<?php $this->display('inc_footer.php') ?>
  <script type="text/javascript">
    $("#membernav").show();
 </script>
   
   
 