<?php $this->display('inc_left.php') ?>
<script type="text/javascript">
function beforeToCash(){
	if(!this.amount.value) throw('请填写提现金额');
	if(!this.amount.value.match(/^[0-9]*[1-9][0-9]*$/)) throw('提现金额错误');
	showPaymentFee()
	var amount=parseInt(this.amount.value);
	if($('input[name=bankId]').val()==2||$('input[name=bankId]').val()==3){
		if(amount<parseFloat(<?=json_encode($this->settings['cashMin1'])?>)) throw('支付宝/微信支付提现最小限额为<?=$this->settings['cashMin1']?>元');
		if(amount>parseFloat(<?=json_encode($this->settings['cashMax1'])?>)) throw('支付宝/微信支付提现最大限额为<?=$this->settings['cashMax1']?>元');
		showPaymentFee()
	}else{
		if(amount<parseFloat(<?=json_encode($this->settings['cashMin'])?>)) throw('提现最小限额为<?=$this->settings['cashMin']?>元');
		if(amount>parseFloat(<?=json_encode($this->settings['cashMax'])?>)) throw('提现最大限额为<?=$this->settings['cashMax']?>元');
		showPaymentFee()
	}
	if(!this.coinpwd.value) throw('请输入资金密码');
	if(this.coinpwd.value<6) throw('资金密码至少6位');
	showPaymentFee()
}

function toCash(err, data){
	if(err){
		alert(err)
	}else{
		reloadMemberInfo();
		$(':password').val('');
		$('input[name=amount]').val('');
		window.location.href="/index.php/cash/toCashResult";
		//alert(data);
		//$.messager.lays(200, 100);
	    //$.messager.anim('fade', 1000);
	    //$.messager.show("<strong>系统提示</strong>", "提款成功！<br />将在10分钟内到账！",0);

	}
}
$(function(){
	$('input[name=amount]').keypress(function(event){
		event.keyCode=event.keyCode||event.charCode;
		
		return !!(
			// 数字键
			(event.keyCode>=48 && event.keyCode<=57)
			|| event.keyCode==13
			|| event.keyCode==8
			|| event.keyCode==46
			|| event.keyCode==9
		)
	});
	
	//var form=$('form')[0];
	//form.account.value='';
	//form.username.value='';
});
</script>
<script type="text/javascript">
function showPaymentFee() {
   $("#ContentPlaceHolder1_txtMoney").val($("#ContentPlaceHolder1_txtMoney").val().replace(/\D+/g, ''));
   jQuery("#chineseMoney").html(changeMoneyToChinese($("#ContentPlaceHolder1_txtMoney").val()));
        }
</script>
 <?php
	$bank=$this->getRow("select * from {$this->prename}member_bank  WHERE uid=? limit 1", $this->user['uid']);
	$this->freshSession();
	$time=strtotime(date('Y-m-d', $this->time));

     $row =$this->getRow("select id,userCoin from {$this->prename}coin_log  WHERE uid=? AND liqType=107 ORDER by id DESC limit 1", $this->user['uid']);
     $min_id = isset($row['id'])?$row['id']:0;
     $diff = $this->checkCoin($this->user['uid'], $min_id, true);

    $rechargeTime=strtotime('00:00');
	$times=$this->getValue("select count(*) from {$this->prename}member_cash where actionTime>=$time and uid=?", $this->user['uid']);
?>
<div class="pagemain">
    <div class="display biao-cont">
 	<?php if($bank['bankId']){?>
<form action="/index.php/cash/ajaxToCash" method="post" target="ajax" datatype="json" onajax="beforeToCash" call="toCash">
<?php
	$key='9cc1ab94e49d22ff';
    $timess=md5(time());
    $token=md5($key.$timess);
?>
	<input name="CANKIF_BOK" type="hidden" value="<?=$timess?>" />
	<input name="TOLKEASF_ASH" type="hidden" value="<?=$token?>" />
<table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
    <tr class='table_b_th'>
      <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>提款申请</td> 
    </tr>
    
    <tr height=25 class='table_b_tr_b' >
      <td align="right" class="copys" height="80" style="color:red;">提示信息：</td>
      <td align="left" ><p>您是尊贵的&nbsp;&nbsp;<strong style="font-size:16px;color:red;">VIP<?=$this->user['grade']?></strong>&nbsp;&nbsp;客户，每天限提&nbsp;&nbsp;<strong style="font-size:16px;color:red;"><?=$this->getValue("select maxToCashCount from {$this->prename}member_level where level=?", $this->user['grade'])?></strong>&nbsp;&nbsp;次,今天您已经成功发起了&nbsp;&nbsp;<strong style="font-size:16px;color:green"><?=$times?></strong>&nbsp;&nbsp;次提现申请</p>
	                    <p>每天的提现处理时间为：<strong style="font-size:16px;color:red;" >
    早上 <?=$this->settings['cashFromTime']?> 至 晚上
    <?=$this->settings['cashToTime']?></strong></p>
	                    <p>提现30分钟内到账。(如遇高峰期，可能需要延迟到两个小时内到帐)</p>
	                    <p style="color:blue;">银行卡用户每日最小提现&nbsp;&nbsp; 
    <strong style="color:green;font-size:16px;"><?=$this->settings['cashMin']?></strong>&nbsp;&nbsp;元，最大提现&nbsp;&nbsp;
    <strong style="color:green;font-size:16px;"><?=$this->settings['cashMax']?></strong>&nbsp;&nbsp;元。
	</td> 
    </tr>
	<tr height=25 class='table_b_tr_b' >
		<?php if($diff == 0){?>
            <td align="right" class="copys">可提现金额：</td>
            <td align="left">&nbsp;<?=$this->user['coin']?></td>
		<?php }else{?>
            <td align="right" class="copys">充值投注：</td>
            <td align="left">还需消费：&nbsp;&nbsp;<?=$diff?> &nbsp;&nbsp;元</td>
		<?php }?>

    </tr>
	<tr height=25 class='table_b_tr_b' >
      <td align="right" class="copys">消费比例说明：</td>
      <td align="left"><p>计算公式：消费比例=投注量/充值额</p>
	                   <p>(消费比例未达到的&nbsp;&nbsp;<strong style="color:red" id="sysbili"><?=$this->settings['cashMinAmount']?></strong>&nbsp;&nbsp;%，则不能提款.)</p></td>
    </tr>

    <tr height=25 class='table_b_tr_b' >
      <td align="right" class="copys" >银行卡账号：</td>
      <td align="left" ><input style="width: 200px" readonly value="<?=$bank['bankId']?>" /></td>
    </tr>
<!--     <tr height=25 class='table_b_tr_b'>
      <td align="right" class="copys">账户名：</td>
      <td align="left" ><input readonly value="<?/*=$bank['username']*/?>" /></td>
    </tr>-->
    <tr height=25 class='table_b_tr_b' >
        <td align="right" class="copys" >支付宝账号：</td>
        <td align="left" ><input style="width: 200px"  readonly value="<?=$bank['account']?>" /></td>
    </tr>
<!--    <tr height=25 class='table_b_tr_b'>
        <td align="right" class="copys">支付宝名：</td>
        <td align="left" ><input readonly value="<?/*=$bank['countname']*/?>" /></td>
    </tr>-->
    <tr height=25 class='table_b_tr_b'>
        <td align="right" class="copys">选择账号：</td>
        <td align="left" ><select name="info" style="margin-left: 10px">
                <option value="银行卡">银行卡</option>
                <option value="支付宝">支付宝</option>
            </select></td>
    </tr>
     <tr height=25 class='table_b_tr_b'>
      <td align="right" class="copys">提款金额：</td>
      <td align="left" ><input name="amount" class="spn9"  value="" id="ContentPlaceHolder1_txtMoney" onkeyup="showPaymentFee();"/>
      <div class="spn12" style="display:inline;" >( 单笔提现限额：最低：&nbsp;<strong style="color:red"><?=$this->settings['cashMin']?></strong>&nbsp;元， 最高：&nbsp;<strong style="color:red"><?=$this->settings['cashMax']?></strong>&nbsp;元 )</div>
      </td> 
    </tr>
	<tr height=25 class='table_b_tr_b'>
      <td align="right" class="copys">提款金额(大写)：</td>
      <td align="left" ><strong style="color:red;margin-left:10px" id="chineseMoney"></strong></td> 
    </tr>
    <tr height=25 class='table_b_tr_b'>
      <td align="right" class="copys">资金密码：</td>
      <td align="left" ><input name="coinpwd" type="password" value="" /></td> 
    </tr>
     <tr height=25 class='table_b_tr_b'>
      <td align="right" style="font-weight:bold;"></td>
      <td align="left"><input type="button" id='put_button_pass' class="btn darwingbtn" style="width:100px" value="提交申请"  onclick="$(this).closest('form').submit()">
        <input type="reset" value="重置" class="btn"/> </td> 
    </tr> 

   
</table> 

</form>

  		<?php }else{?>
            <div style=" margin-top:30px; text-align:center;">尚未设置您的银行账户！&nbsp;&nbsp;<a href="/index.php/safe/info" style="color:#F00; text-decoration:none;">马上设置>></a></div>
        <?php }?>

    </div>

</div>
<!--以下为模板代码-->
<?php $this->display('inc_footer.php') ?>
   
 