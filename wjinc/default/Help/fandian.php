<!DOCTYPE html>
<html>
<head lang="en">
<?php $this->display('inc_base.php',0, $this->settings['webName'] . '祝您财运享通') ?>
<style type="text/css">
#help_content{
margin-left:100px;
}
#help_page_link{
	color:#0000ff;
	font-size:14px;
	margin: 5px 0 0 18px;
}
#help_page_solgan{
	color:#7c7c7c;
	font-size:18px;
	margin: 0px 0 0 18px;
	padding-top:20px;
}
#help_page_container{
	width:1180px;
	border:1px solid #CCC;
	margin: 10px 18px;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
}
#help_page_container table{
	width:1150px;
	margin:10px auto;
}
.table_text{
	font-size:14px;
}
.table_text02{
	font-size:14px;
	color:#006;
	font-weight:bold;
}
.table_text03{
	font-size:14px;
	color:#666;
	position:relative;
	left:20px; 
}
.table_text tr{
	border-top:#fff 2px solid;
	border-bottom:#fff 2px solid;

}
.table_text td{
	border-left:#fff 2px solid;
	border-right:#fff 2px solid;
	padding:5px 10px 5px 10px;
	line-height:20px;

}.help-seep {
	margin-top: 9px;
	margin-bottom: 20px;
	margin-left: 37px;
}

.help-seep li {
	float: left;
	margin-right: 19px;
	padding: 5px;
	width: 252px;
	height: 127px;
	border: 1px solid #f3f4f4;
}

.help-seep li .seep-item {
	padding: 24px 5px 0 20px;
	height: 103px;
	background: #f3f4f4;
}

.help-seep li .seep-item a {
	display: inline-block;
	margin-top: 20px;
	margin-left: 10px;
	color: #4a9cdc;
	text-align: center;
}

.help-seep li .seep-item em {
	line-height: 30px;
}

.help-seep li .ico {
	float: left;
	display: inline-block;
	margin-right: 10px;
	width: 60px;
	height: 62px;
	background: url("/images/new/help_ico01.png") no-repeat;
}

.help-seep li .ico01 {
	background-position: 0 0;
}

.help-seep li .ico02 {
	background-position: 0 -65px;
}

.help-seep li .ico03 {
	background-position: 0 -132px;
}

.help-seep li .ico04 {
	background-position: 0 -197px;
}

.help-seep li .desc {
	float: left;
}
</style>
</head>
<?php $this->display('inc_header.php') ?>
   <div id="help_content">
    <div class="help-seep clearfix">
        <ul>
            <li>
                <div class="seep-item">
                    <span class="ico ico03"></span>
                    <span class="desc">
                        <em class="c64 fs12">&nbsp;&nbsp;开奖时间、期数说明。仅供参考。</em><br>
                    </span>
                     <a href="/index.php/helpkj"  class="clearfix">开奖说明</a>
                </div>
            </li>
           
            <li>
                <div class="seep-item">
                    <span class="ico ico03"></span>
                    <span class="desc">
                        <em class="c64 fs12">重庆时时彩、江西时时彩、新疆时时彩</em><br>
                    </span>
                     <a href="/index.php/helpssc"  class="clearfix">时时彩玩法</a>
                </div>
            </li>
           
            <li>
                <div class="seep-item">
                    <span class="ico ico03"></span>
                    <span class="desc">
                        <em class="c64 fs12">重庆时时彩、黑龙江时时彩、天津时时彩</em><br>
                    </span>
                     <a href="/index.php/help115"  class="clearfix">11选5玩法</a>
                </div>
            </li>
            <li>
                <div class="seep-item">
                    <span class="ico ico03"></span>
                    <span class="desc">
                        <em class="c64 fs12">&nbsp;&nbsp;返点比例模式折算表格（大约）</em><br>
                    </span>
                     <a href="/index.php/helpfandian"  class="clearfix">返点模式</a>
                </div>
            </li>
        </ul>
    </div>
            <div id="help_page_container">
                <table width="940" border="0" cellpadding="0" cellspacing="0" cols="4" frame="VOID" rules="NONE" class="table_text">

                    <tbody>
                        <tr>
                            <td width="161" align="center" valign="middle" sdnum="1028;1028;General">返点比例</td>
                            <td width="162" align="center" valign="middle" sdnum="1028;1028;General">折算模式</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">13%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1960</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.9%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1958</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.8%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1956</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.7%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1954</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.6%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1952</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.5%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1950</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.4%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1948</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.3%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1946</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.2%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1944</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12.1%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1942</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">12%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1940</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">11%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1920</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">10.5%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1910</td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" bgcolor="#d7f3ff" sdnum="1028;1028;General">10%</td>
                            <td align="center" valign="middle" bgcolor="#ecffcc" sdnum="1028;1028;General">1900</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
<?php $this->display('inc_footer.php') ?>
</body>
</html>
