

<?php $this->display('inc_header.php') ?>

<style>

</style>

<link rel="stylesheet" href="/skin/new/css/index-1.css">
<div class="clearfix pr">
    <ul class="about-tab">
        <li class="active">关于我们<i class="about-tab-ico"></i></li>
    </ul>
</div>

<div class="clearfix" style="padding:20px 10px 60px 10px;background-color: #e6e9ed">
    <div>
        <em class="fs22 fee" style="margin:0 0 20px 5px">网站实时数据</em>
    </div>
    <div class="fl" style="width: 70%">
        <span class="clearfix" style="position:relative;top: 15px">
        <i class="fl ico-01" ></i>
        <p class="fs18 c64" style="margin-left:80px" >注册用户</p>
        <p class="fs18 c64" style="margin-left:80px" ><?=$this->settings['totalcount'] ?>人</p>
    </span>
        <span class="clearfix" style="position:relative;top: 35px">
        <i class="fl ico-02"></i>
        <p class="fs18 c64" style="margin-left:80px" >累计中奖</p>
        <p class="fs18 c64" style="margin-left:80px" ><?=$this->settings['totalcoin'] ?>元</p>
    </span>
    </div>

    <div class="fl"  style="width:30%">
        <img  style="display:block;margin-left: -50px;width: 150px" src="/images/new/s03.png" />
    </div>
</div>




<div class="clearfix" style="padding: 30px 10px;background-color: #FFF">
        <div class="fl" style="width: 20%;margin-right: 15px">
            <img src="/images/new/s04-1.png" style="width: 100%" alt="">
        </div>
        <div class="fl" style="width: 75%">
            <h2 class="fs26 fee">网站介绍</h2>
            <p style="font-size: 14px;margin-top: 10px"><?= $this->settings['webName'] ?>于2016年1月正式上线，是国内最大的数字彩票交易平台，是一家拥有10000用户选择和信任的互联网平台。众多的合作伙伴证明我们的实力和价值，易宝第三方支付、支付宝、微信支付等。因为有他们，我们才可以更好的为广大用户提供最优质可靠的技术产品服务。
                <span><?= $this->settings['webName'] ?>坚持“让每一个用户简单赚钱”的使命，努力并坚持不懈。</span>
            </p>
        </div>

</div>

<div class=" clearfix" style="padding: 20px 10px">
    <div>
        <div class="fl">
            <h2 class="fs26 fee">愿景和使命</h2>
            <p style="font-size: 14px;margin-top: 10px">
                愿景:<br>
                1.成为一个持续发展的品牌<br>
                2.想赚钱就想到<?= $this->settings['webName'] ?><br>
                3.可以为500万用户创造价值<br>
                使命：
                <br>
                让每一个人简单的赚钱！
            </p>
        </div>
        <div class="fl" style="margin-top: 30px">
            <img src="/images/new/s06.png" style="display:block;width: 120px" />
        </div>

    </div>
</div>
<div class="con-item about-contact about-mod clearfix" style="display: none">
    <div class="w1200">
        <div class="fl about-contact-con">
            <h2 class="fs26 fee">联系我们</h2>
            <p>
                客户服务邮箱：kefu@<?= $this->settings['webName'] ?><br>
                商务合作QQ: cp003853@outlook.com<br>
                邮箱：service@<?= $this->settings['webName'] ?><br>
                网址：http://www.5888bj.com.com/
            </p>
        </div>
    </div>
</div>


<?php $this->display('inc_footer.php') ?>
