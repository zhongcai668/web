<?php
$this->getTypes();
$this->getPlayeds();

// 日期限制
$timeWhere=' l.actionTime between '. strtotime('-1 year').' and '.time();
// 帐变类型限制
$liqTypeWhere = '';
if($_REQUEST['liqType']){
	$liqTypeWhere=' AND liqType='.intval($_REQUEST['liqType']);
	if($_REQUEST['liqType']==2) $liqTypeWhere=' AND  liqType between 2 and 3';
}


//用户限制
$userWhere=" and l.uid={$this->user['uid']}";

// 冻结查询
$this->pageSize = 12;
$sql="select b.type, b.playedId, b.actionNo, b.mode, l.liqType, l.coin, l.fcoin, l.userCoin, l.actionTime, l.extfield0, l.extfield1, l.info from  {$this->prename}coin_log l left join {$this->prename}bets b on b.id=extfield0 where  $timeWhere $liqTypeWhere $userWhere  and l.liqType not in(4,11,104) order by l.id desc";

$list=$this->getPage($sql, $this->page, $this->pageSize);
$params=http_build_query($_REQUEST, '', '&');
$modeName=array('2.00'=>'元', '0.20'=>'角', '0.02'=>'分');
$liqTypeName=array(
	1=>'账户充值',
	2=>'游戏返点',
	3=>'返点分红',//分红
	//4=>'抽水金额',
	5=>'停止追号',
	6=>'中奖金额',
	7=>'撤单返款',
	8=>'提现失败返回冻结金额',
	9=>'管理员充值',
	10=>'解除抢庄冻结金额',
	//11=>'收单金额',
	12=>'上级充值',
	13=>'上级充值成功扣款',
	50=>'签到赠送',
	51=>'首次绑定工行卡赠送',
	52=>'充值佣金',
	53=>'消费佣金',
	54=>'充值赠送',
	55=>'注册佣金',
	56=>'亏损佣金',
	100=>'抢庄冻结金额',
	101=>'投注冻结金额',
	102=>'追号投注',
	103=>'抢庄返点金额',
	//104=>'抢庄抽水金额',
	105=>'抢庄赔付金额',
	106=>'提现冻结',
	107=>'提现成功扣除冻结金额',
	108=>'开奖扣除冻结金额',
	109=>'上级充值',
	110=>'上级充值成功扣款',
	111=>'积分兑换',
	201=>'余额转入',
	202=>'余额转出',
	203=>'余额收益'
);

?>
<div>
    <table width="100%" class='table_b'>
        <thead>
        <tr class="table_b_th">
            <td>时间</td>
            <td>帐变类型</td>
            <td>资金</td>
            <td>余额</td>
        </tr>
        </thead>
        <tbody class="table_b_tr">
		<?php if($list['data']) foreach($list['data'] as $var){ ?>
            <tr>
                <td><?= date('m-d H:i', $var['actionTime'])?></td>
                <td><?=$liqTypeName[$var['liqType']]?></td>
                <td><?=number_format($var['coin'],2)?></td>
                <td><?=$var['userCoin']?></td>
            </tr>
		<?php } ?>
        </tbody>
    </table>
	<?php
	$this->display('inc_page.php',0,$list['total'],$this->pageSize, "/index.php/{$this->controller}/{$this->action}-{page}/{$this->type}?$params");
	?>
</div>

<script>
    $(function(){
        $('.bottompage a').click( function(){
            $('#biao-cont').load($(this).attr('href'));
            return false;
        });
    });
</script>