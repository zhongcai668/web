
<?php $this->display('inc_header.php'); ?>

<link rel="stylesheet" href="/skin/new/css/index.css">
<style>
    .product{
        margin-top: 10px;
    }

    .product li{
        width: 100%;
    }
    .rank_list{
        margin-top: 140px;
    }
    .btn{
        padding: 0 7px;
    }
</style>

<script type="text/javascript">
    function IMGS(id,w,h,da,c){
        this.w=w;
        this.h=h;
        this.da=da;
        this.l=da.length;
        this.html="";
        this.id=id;
        this.child={img:[],div:[]};
        this.cr=0;
        this.xx=0;
        this.m=10;
        this.pr=null;
        this.time=false;
        if(typeof c =="undefined"){
            this.c={bg:"#600",color:"#fff",cbg:"#eee",ccolor:"#600",bc:"#ccc",step:6000}
        }else{
            this.c=c;
        }
        this.step=this.c.step;
        this.init();
    }
    IMGS.prototype={
        init:function(){
            document.writeln('<div id="'+this.id+'" style="position:relative;width:'+this.w+'px;height:'+this.h+'px;"></div>');
            var Bdiv=document.createElement("div");
            Bdiv.style.cssText="position:absolute;right:10px;bottom:1px;height:25px;z-index:100;";
            this.$(this.id).appendChild(Bdiv);
            var self=this;
            for(var i=this.l-1;i>=0;i--){
                var img=document.createElement("img");
                var div=document.createElement("div");
                img.width=this.w;
                img.height=this.h;
                img.border="0";
                img.src=this.da[i].src;
                img.alt=this.da[i].name;
                div.innerHTML=(i+1);
                div.style.cssText="float:right;width:20px;height:18px;margin-right:5px;border:1px solid "+this.c.bc+";line-height:18px;text-align:center;cursor:default";
                if(i>0){
                    div.style.cssText+="background:"+this.c.bg+";color:"+this.c.color+";";
                }else{
                    div.style.cssText+="background:"+this.c.cbg+";color:"+this.c.ccolor+";";
                }
                if(typeof this.da[i].link !=""){
                    var a=document.createElement("a");
                    a.href=this.da[i].link;
                    a.appendChild(img);
                    this.child.img[i]=a;
                    this.$(this.id).appendChild(a);
                }else{
                    this.child.img[i]=img;
                    this.$(this.id).appendChild(img);
                }
                this.child.img[i].style.cssText+=";position:absolute;left:0px;top:0px;z-index:1;display:none";
                this.child.div[i]=div;
                div.onmouseover=function(){
                    self.Cl(parseInt(this.innerHTML)-1);
                }
                if(this.l>0)this.pr=this.child.img[0];
                if(this.l>1)Bdiv.appendChild(div);
            }
            this.Go();
        },
        Go:function(){
            var self=this;
            for(var i=0;i<this.l;i++){
                this.child.img[i].style.zIndex="1";
                this.child.div[i].style.background=this.c.bg;
                this.child.div[i].style.color=this.c.color;
            }
            if(this.pr!=null)this.pr.style.zIndex="10";
            this.pr=this.child.img[this.cr];
            this.m=10;
            with(this.child.img[this.cr].style){filter='alpha(opacity=10)';opacity=0.1;display="";zIndex="20";}
            this.None(self,this.child.img[this.cr]);
            with(this.child.div[this.cr].style){background=this.c.cbg;color=this.c.ccolor;}
            this.cr++;
            if(this.cr==this.l)this.cr=0;
            if(this.l>1)this.time=setTimeout(function(){self.Go()},this.step);
        },
        None:function(self,tt){
            with(tt.style){filter='alpha(opacity='+self.m+')';opacity=(self.m/100);}
            self.m+=5;
            if(self.m<101)
                self.xx=setTimeout(function(){self.None(self,tt)},10);
            else
                clearInterval(self.xx);
        },
        Cl:function(cr){
            clearInterval(this.xx);
            clearTimeout(this.time);
            this.cr=cr;
            this.Go();

        },
        $:function(id){return document.getElementById(id);}
    }

    var imgs=[];

    imgs.push({src:"/images/1.jpg",name:"第一张",link:"/images/1.jpg"});
    imgs.push({src:"/images/2.jpg",name:"第二张",link:"/images/2.jpg"});
    var c={
        bg:"#333",//右下数字背景颜色
        color:"#FFF",//右下数字颜色
        cbg:"#FFF",//右下当前数字背景颜色
        ccolor:"#333",//右下当前数字颜色
        bc:"#ccc",//边框颜色
        step:6000 //步长
    };
    new IMGS("TopI",$(window).width(),180,imgs,c);
    //new IMGS("id号随便写不要和页面的id重复就可以",宽度,高度,数据[,设置]);
</script>

<div class="product clearfix">
    <ul>
        <li class="absorbed">
            <i></i>
            <h3 class="title">专业</h3>
            <p>
                我们只做官方数字彩<br />
                因为专注，所以更专业!
            </p>
        </li>
        <li class="sincerity">
            <i></i>
            <h3 class="title">诚信</h3>
            <p>
                致力打造健康购彩环境<br />
                为广大彩民提供便捷、优质的购彩服务
            </p>
        </li>
    </ul>
</div>
<?php $data=$this->getRows("select left(m.username,3) as shortname,c.uid,SUM(c.coin)+380000 as total from {$this->prename}coin_log c inner join {$this->prename}members m on m.uid=c.uid  where c.liqType=6 group by c.uid order by total desc limit 8");  //查询前十条最高中奖金额数据

?>

    <div class="rank_list"  style="font-size:13px;">
        <table class="table table-bordered table-condensed">
            <tbody><tr class="success"><td colspan="3" style="text-align:center; font-size:14px">中奖排行</td></tr>
            <tr style=" font-size:14px">
                <td>序号</td>
                <td>用户账户</td>
                <td>中奖金额</td>
            </tr>
            <tr>
                <td><a class="btn btn-mini btn-danger disabled">1</a></td><td><?=$data[0]['shortname'].'***' ?></td>
                <td><?=$data[0]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini btn-warning disabled">2</a></td><td><?=$data[1]['shortname'].'***' ?></td>
                <td><?=$data[1]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini btn-success disabled">3</a></td><td><?=$data[2]['shortname'].'***' ?></td>
                <td><?=$data[2]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini">4</a></td><td><?=$data[3]['shortname'].'***' ?></td>
                <td><?=$data[3]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini">5</a></td><td><?=$data[4]['shortname'].'***' ?></td>
                <td><?=$data[4]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini">6</a></td><td><?=$data[5]['shortname'].'***' ?></td>
                <td><?=$data[5]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini">7</a></td><td><?=$data[6]['shortname'].'***' ?></td>
                <td><?=$data[6]['total'] ?></td>
            </tr>
            <tr>
                <td><a class="btn btn-mini">8</a></td><td><?=$data[7]['shortname'].'***' ?></td>
                <td><?=$data[7]['total'] ?></td>
            </tr>
            <!--<tr>
				<td><a class="btn btn-mini">9</a></td><td><?=$data[8]['shortname'].'***' ?></td>
				<td><?=$data[8]['total'] ?></td>
			</tr>
			<tr>
				<td><a class="btn btn-mini">10</a></td><td><?=$data[9]['shortname'].'***' ?></td>
				<td><?=$data[9]['total'] ?></td>
			</tr>-->
            </tbody></table>
    </div>




<?php $this->display('inc_footer.php'); ?>
	
