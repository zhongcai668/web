<?php $this->display('inc_header.php'); ?>
<?php

$lastNo=$this->getGameLastNo($this->type);
$times = time();
$actionNo=$this->getGameNo($this->type);
$types=$this->getTypes();
?>
<?php $this->display('modal.php') ?>
<link href="/skin/new/css/lottery.css" rel="stylesheet" type="text/css" />
<link href="/skin/main/skins.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/skin/js/gamecommon.js"></script>
<script type="text/javascript" src="/skin/main/game.js"></script>
<script type="text/javascript" src="/skin/js/Array.ext.js"></script>
<script type="text/javascript" src="/skin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/skin/main/function.js"></script>
<script src="/skin/js/jquery.simplemodal.src.js"></script>


<style>
    .lotMain {
        height: 130px;
    }

    .lotMain .brand-img{
        float: left;
    }

    .lotMain .brand-img img{
        margin-left: 10px;
        margin-top: 10px;
        margin-right: 20px;
        width: 80px;
    }
    .lotMain .gamediv{
        font-family: "微软雅黑", "Microsoft Sans Serif";
        color: #E9573F;
        margin-left: 14px;
        font-weight: bold;
    }
    .brand-info p{
        padding-top: 10px;
    }
    .brand-info span{
        color: #E9573F;
        font-weight: bold;
    }

    .num-table .pp .title {
        width: 50px;
    }

    .game .pp input.code{
        margin-right: 0;
    }
    .game .pp input.action{
        margin: 12px 2px 0 0;

    }
    .game-main .game-btn .ul-li {
        padding: 3px 9px 0 9px;
    }

    .game-btn2 .ul-li{
        width: 88px;
    }
    .clear{
        clear: both;
    }
    .betSubmit{
        padding-left: 17px;
    }
    .betSubmit .beiBox{
        width: 108px;
    }
    .betSubmit .beiBox input{
        width: 45px;
    }
    .betSubmit .beiBox button {
        width: 29px;
    }
    .codeList{
        width: 100%;
    }
    .touzhu-cont{
        width: 100%;
    }

    .jixuanBox{
        width: 90%;
    }
    .betSubmit{
        height: 40px;
        line-height: 40px;
    }
    .betSubmit label{
        display: inline;
    }
    .todayNumber{
        width: 100%;
    }
    .todayNumber .issul li{
        width: 100%;
    }
    .todayNumber .issul{
        width: 100%;
    }
    .todayNumber .issul li span{
        width: 40%;
    }

</style>

<div class="lotMain">
    <!-- 开奖信息 -->
        <div class="brand-img">
            <?php if($types[$this->type]['type']==2) { //11选5?>
                <img src="/images/index/c4.png" class="gameimg" />
                <div class="gamediv"><span><?=$types[$this->type]['title']?></span></div>
            <?php }else if($types[$this->type]['type']==6){ //北京赛车?>
                <img src="/images/index/c7.png" class="gameimg"/>
                <div class="gamediv"><span><?=$types[$this->type]['title']?></span></div>
            <?php }else if($types[$this->type]['type']==4){ //快乐十分?>
                <img src="/images/index/c9.png" class="gameimg" />
                <div class="gamediv"><span><?=$types[$this->type]['title']?></span></div>
            <?php }else if($types[$this->type]['id']==9){ //福彩3D?>
                <img src="/images/index/3d.png" class="gameimg" />
                <div class="gamediv" style="margin-left:10px;"><span><?=$types[$this->type]['title']?></span></div>
            <?php }else if($types[$this->type]['id']==10){ //排列三?>
                <img src="/images/index/p3.png" class="gameimg" />
                <div class="gamediv" style="margin-left:10px"><span><?=$types[$this->type]['title']?></span></div>
            <?php }else{ ?>
                <img src="/images/index/c1.png" class="gameimg"/>
                <div class="gamediv"><span><?=$types[$this->type]['title']?></span></div>
            <?php } ?>

        </div>
        <div class="brand-info">
            <p>第 <span><?=$actionNo['actionNo']?></span> 期</p>
            <p>正在销售中，&nbsp;&nbsp;截止时间</p>
            <p><span><?=$actionNo['actionTime']?></span></p>
        </div>

</div>
    <!-- 开奖信息 end -->
<div class="game">
    <!--游戏body-->
    <?php $this->display('index/inc_game.php'); ?>
    <!--游戏body  end-->
</div>
<script type="text/javascript">
    var game={
            type:<?=json_encode($this->type)?>,
            played:<?=json_encode($this->played)?>,
            groupId:<?=json_encode($this->groupId)?>
        },
        user="<?=$this->user['username']?>",
        aflag=<?=json_encode($this->user['admin']==1)?>;
</script>


<?php $this->display('inc_footer.php');  ?>


