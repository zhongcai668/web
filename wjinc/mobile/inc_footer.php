<script>
    $('.navbar-toggle').on('click', function(){
        var header = $("#bs-example-navbar-collapse-1");
        if(header.hasClass("collapse")) {
            header.removeClass("collapse");
        } else {
            header.addClass("collapse");
        }

    });

    $(function(){
        function footerPosition(){
            var mobile_footer = $("#mobile-footer");
            mobile_footer.removeClass("fixed-bottom");
            var contentHeight = document.body.scrollHeight,//网页正文全文高度
                winHeight = window.innerHeight;//可视窗口高度，不包括浏览器顶部工具栏
            if(!(contentHeight > winHeight)){
                //当网页正文高度小于可视窗口高度时，为footer添加类fixed-bottom
                mobile_footer.addClass("fixed-bottom");
            } else {
                mobile_footer.removeClass("fixed-bottom");
            }
            var userclass = $("#userclass");
            var width = window.innerWidth;
            if(width > 370) {
                userclass.css('width', '90%');
            } else {
                userclass.css('width', '95%');
            }
        }
        footerPosition();
        $(window).resize(footerPosition);
    });
</script>
<div id="wanjinDialog"></div>

<div id="mobile-footer" style="background-color: #e6e9ed;padding-top: 8px">
    <img style="width: 100%" src="/images/footer_base.png" alt="">
    <div style="text-align: center;">
        <p style="padding: 4px">
            <a target="_blank" href="/"><?=$this->settings['webName']?> - 专业、诚信、标准的购彩平台 </a>
        </p>

        <p style="padding-bottom: 4px">
            版权所有：<?=$this->settings['webName']?>©2015-2017<br>
        </p>
    </div>
</div>
<style>
    .fixed-bottom {position: fixed;bottom: 0;}
</style>
</body>
</html>