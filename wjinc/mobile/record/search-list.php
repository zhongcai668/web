<!--下注列表-->
<?php
$sql="select b.* from {$this->prename}bets b";
$sql.=' order by id desc';
$this->pageSize = 12;
$data=$this->getPage($sql, $this->page, $this->pageSize);
?>
<table width="100%" class='table_b'>
    <thead>
    <tr class="table_b_th">
        <td>下注时间</td>
        <td>彩种</td>
        <td>期号</td>
        <td>总额</td>
        <td>奖金</td>
        <td>状态</td>

    </tr>
    </thead>
    <tbody class="table_b_tr" >
	<?php if($data['data']){
		foreach($data['data'] as $var){ ?>
            <tr>
                <td><?=date('m-d H:i', $var['actionTime'])?></td>
                <td><?=$this->ifs($this->types[$var['type']]['shortName'],$this->types[$var['type']]['title'])?></td>
                <td><?=$var['actionNo']?></td>
                <td><?=$var['mode']*$var['beiShu']*$var['actionNum']*($var['fpEnable']+1)?></td>
                <td><?=$this->iff($var['lotteryNo'], number_format($var['bonus'], 2), '0.00')?></td>
                <td>
					<?php
					if($var['isDelete']==1){
						echo '<font color="#999999">已撤单</font>';
					}elseif($var['status'] == 0){
						echo '<font color="#009900">未开奖</font>';
					}elseif($var['status'] == 1){
						echo '<font color="red">已开奖</font>';
					}else{
						echo '进行中';
					}
					?>
                </td>

            </tr>
		<?php } }else{ ?>
        <tr><td colspan="12">暂无投注信息</td></tr>
	<?php } ?>
    </tbody>
</table>
<?php
$this->display('inc_page.php',0,$data['total'],$this->pageSize, "/index.php/{$this->controller}/{$this->action}-{page}");
?>

<script>
    $(function(){
        $('.bottompage a').click( function(){
            $('#biao-cont').load($(this).attr('href'));
            return false;
        });
    });
</script>
