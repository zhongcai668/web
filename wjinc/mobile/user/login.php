<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
</head>
<style>

    .login .btn {
        width: 100%;
    }
    .login {
        width: 90%;
        margin: 0 auto;
    }
    .nav-header li{
        list-style-type: none;
        float: left;
        background-color: #E9573F;
        text-align: center;
    }
    .nav-header a{
        font-size: 16px;
        color: #FFce54;
        font-weight: bold;
    }
</style>
<body>

<div class="login">
    <div style="text-align: center;margin: 70px auto;">
        <img src="/images/new/<?=$this->settings['cashPersons']?>" alt="">
    </div>
    <form method="post" id="login-form">

        <input  type="hidden" name="pu" value="<?=$args[0]['uid']?>"  placeholder="">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                <input class="form-control" type="text" name="u"  placeholder="请输入用户名">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                <input class="form-control" type="password" name="p" placeholder="请输入密码">
            </div>
        </div>
        <div id="alert-text" style="height: 20px;text-align: center;color: #ff3333"></div>
        <button class="btn btn-danger" type="button" id="login-btn">登 录</button>
    </form>
</div>
<script src="https://apps.bdimg.com/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $("#login-btn").click(function(e){
            $.ajax({
                type: "post",
                url: "/user/lo",
                data: $("#login-form").serialize(),
                dataType: "json",
                success: function (data) {
                    if(data) {
                        $('#alert-text').text(data);
                        setTimeout(function () {
                            $('#alert-text').text('');
                        }, 2000)
                    } else {
                        window.location.href = '/';
                    }
                }
            });
        });
    })
</script>
</body>
</html>
