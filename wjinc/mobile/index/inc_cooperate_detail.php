<?php $this->title =  $this->settings['webName'] ?>
<?php $this->display('inc_header.php') ?>
<link rel="stylesheet" href="/skin/css/order.css" />
<link rel="stylesheet" href="/skin/css/detail.css" />
<script language="javascript" type="text/javascript" src="/js/cooperate.js"></script>
<style>
    .ulTable {
        width: 352px;
        height: 54px;
        padding:0;
        overflow: hidden;

        border: 1px solid #d7d7d7;
        background: url(/Images/btn_repeat.png?2741409559cf194be56d038f2b397b77) repeat-x 0 -255px;
        border-radius: 4px;
    }
    .scheme{
        width: 100%;
    }
    .ulTable li{
        width: 25%;
    }
</style>
<div class="header clearfix">
    <div class="menu">
        <div class="docBody clearfix">

            <div id="mainBody"> <div class="game_header clearfix">
                    <div class="headerBox">
                        <div class="det_t_bg">
                            <div class="s-logo Ssc-logo"></div>
                            <div class="clearfix titleBox">
                                <h1>重庆时时彩</h1>
                                <div class=abstract>
                                    <span>发起时间：<?= date('Y-m-d H:i:s', $this->order['actionTime'])?></span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="topWrap clearfix">
                    <div class="userInfoBox">
                        <div class="clearfix userInfo">
                            <img style="display:inline;float:left;margin:0 15px 0 10px;border:1px solid #C2C2C2;height:60px;width:60px;"
                                 src="<?= $this->order['user_info']['avatar_url']?>" />
                            <div >
                                <span>发起人：<strong id="username"><?= $this->order['user_info']['nickname']?></strong></span>
                                <div class="textL">个人战绩：<em  title="<?=$this->order['user_info']['scoreTotal']?>" id="user-level"></em></div>
                            </div>
                        </div>


                    </div>

                    <div class="scheme">
                        <ul style="margin-left: 10px">

                            <li  style="font-weight:bold">方案进度： <? $finish_rate = round(($this->order['total_volume'] - $this->order['available_volume'])/$this->order['total_volume'] * 100, 2)?>
                                <div class="progressBox" style="width: 300px;position: relative;left: 0">
                                    <div class="progressBar">
                                        <span style="width:<?=$finish_rate?>%" class="progress"><strong><?=$finish_rate?></strong>%</span>
                                        <i style="right:50.00%"></i>
                                    </div>
                                </div></li>

                        </ul>
                        <ul id="centerh" class="ulTable clearfix" >
                            <li><span>总金额</span><strong><?= $this->order['total_amount']?>元</strong></li>
                            <li><span>剩余份数</span><strong><?= $this->order['available_volume']?> 份</strong></li>
                            <li><span>总份数</span><strong><?= $this->order['total_volume']?> 份</strong></li>
                            <li style="border-right:0;">
                                <span>中奖金额</span>
                                <strong id="winmoney">
                                    <?php if($this->order['status']){ ?>
                                    <span style="color: <?= $this->order['bonus'] > 0 ?'#FF0000' : '#228B22'?>"><?= $this->order['bonus'] > 0 ? $this->order['bonus'] : '未中奖'?></span>
                                    <?php } else { ?>
                                        <span style="color: #666">未开奖</span>
                                    <?php } ?>
                                </strong></li>
                        </ul>
                    </div>
                    <div id="tzot" style="padding:10px">
                        <table cellspacing='0' cellpadding='0' width='100%' border='0' id='gaopinNumberTable' class='user_table' style='height:100px'>
                            <thead>
                            <tr><td>投注号码</td></tr>
                            <?php if($this->order['public_level']){ ?>
                            <tr><td style="background:#FFFFFF; padding:10px; text-align:center;"><?= strtr($this->order['actionData'], [' ' => ','])?></td></tr>
                            <?php } else { ?>
                            <tr><td style="background:#FFFFFF; padding:10px; text-align:center;">该方案选择永久保密（仅方案发起人可见）</td></tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div id="paybox" class="paybox">
                        <?php if($this->order['status']){ ?>
                            <div id="jiezhi">
                                <a class="end_Btn" href="javascript:void (0);" rel="nofollow" title="方案已截止"></a>
                                <a  href="/index/cooperateHall">您可以选择参加其他合买>></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="number_user_wrap">
                    <!--选号详情 start -->
                    <div id="show_list_div">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="user_table hmxq">
                            <thead>
                            <tr>
                                <td width="10%">期号</td>
                                <td width="10%">金额</td>
                                <td width="10%">倍数</td>
                                <td width="10%">奖金</td>
                                <td width="10%">状态</td>

                            </tr>

                            <tbody>
                            <tr>
                                <td><?= $this->order['actionNo']?></td>
                                <td>￥<?= $this->order['total_amount']?></td>
                                <td>
                                    <?= $this->order['beiShu']?>
                                </td>

                                <td><?= $this->order['bonus']?></td>
                                <td>
                                    <?php if($this->order['status']){ ?>
                                        <span style="color: <?= $this->order['bonus'] > 0 ?'#FF0000' : '#228B22'?>"><?= $this->order['bonus'] > 0 ? '已中奖' : '未中奖'?></span>
                                    <?php } else { ?>
                                        <span style="color: #666">未开奖</span>
                                    <?php } ?>
                                </td>

                            </tr>
                            </tbody>

                            </thead>
                        </table>
                        <!--选号详情 start -->
                        <ul class="number_user_tab clearfix" >
                            <li class="an_cur"><a href="javascript:void(0);">参与用户</a></li>
                        </ul>
                        <!--合买参与用户 start -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="user_table" >
                            <thead>
                            <tr>

                                <th width="20%" style="text-align: center">用户名</th>
                                <th width="20%" style="text-align: center">认购金额</th>
                                <th width="20%" style="text-align: center">中奖金额</th>
                                <th width="40%" style="text-align: center">参与时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $sql="select * from {$this->prename}hm where bet_id={$this->order['id']} AND status=1 order by id";
                            $groups=$this->getRows($sql);

                            foreach($groups as $k => $group){
                                ?>
                                <tr>
                                    <td><?= $this->getName($group['nickname'])?></td>
                                    <td>￥<?= $group['buy_amount']?></td>
                                    <td>￥<?= $group['bonus_amount']?></td>
                                    <td><?= $group['created_at']?></td>
                                </tr>
                            <?php } ?>


                            </tbody>

                        </table>
                        <!--合买参与用户 end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var  user_level = $("#user-level");
    user_level.html(Record_(user_level.attr('title')));
</script>

<?php $this->display('inc_footer.php') ?>














