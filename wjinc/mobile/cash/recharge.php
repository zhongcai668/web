<?php $this->display('inc_header.php') ?>
<style>
    #td-btn{
        height: 60px;
        padding-left: 10px;

    }
    #put_button_pass {
        height: 30px;
        line-height: 30px;
        width: 80px;
    }
</style>
<script type="text/javascript" src="/skin/js/jquery.cookie.js"></script>
<script type="text/javascript">
    $(function(){
        $('form').trigger('reset');
        $(':radio').click(function(){
            var data=$(this).data('bank'),
                box=$('#display-dom');

            $('#bank-type-icon', box).attr('src', '/'+data.logo);
            //$('#bank-link', box).attr('href', data.home);
            //$('#bank-account', box).val(data.account);
            //$('#bank-username', box).val(data.username);
            //$('.example2', box).attr('rel', data.rechargeDemo);

            if($.cookie('rechargeBank')!=data.id) $.cookie('rechargeBank', data.id, 360*24);
        });
        $('.example2').click(function(){
            var src='/'+$(this).attr('rel');
            if(src) $('<img>',{src:src}).css({width:'640px',height:'480px'}).dialog({width:660,height:500,title:'充值演示'});
        });
        var bankId=$.cookie('rechargeBank')||$(':radio').attr('value');
        $(':radio[value='+bankId+']').click();

        $('.copy').click(function(){
            var text=document.getElementById($(this).attr('for')).value;
            if(!CopyToClipboard(text, function(){
                    alert('复制成功');
                }));
        });

        $('.example2').click(function(){
            var src='/'+$(this).attr('rel');
            if(src) $('<div>').append($('<img>',{src:src,width:'640px',height:'480px'})).dialog({width:630,height:500,title:'充值演示'});
        });
    });
    function changeMoneyToChinese(a) {
        var o = new Array("\u96f6", "\u58f9", "\u8d30", "\u53c1", "\u8086", "\u4f0d", "\u9646", "\u67d2", "\u634c", "\u7396");
        var l = new Array("", "\u62fe", "\u4f70", "\u4edf");
        var k = new Array("", "\u4e07", "\u4ebf", "\u5146");
        var h = new Array("\u89d2", "\u5206", "\u6beb", "\u5398");
        var d = "\u6574";
        var g = "\u5143";
        var b = 1000000000000000;
        var c;
        var e;
        var j = "";
        var f;
        if (a == "") { return "" }
        a = parseFloat(a);
        if (a >= b) {
            alert("\u8d85\u51fa\u6700\u5927\u5904\u7406\u6570\u5b57");
            return ""
        }
        if (a == 0) {
            j = o[0] + g + d;
            return j
        }
        a = a.toString();
        if (a.indexOf(".") == -1) {
            c = a;
            e = ""
        } else {
            f = a.split(".");
            c = f[0];
            e = f[1].substr(0, 4)
        }
        if (parseInt(c, 10) > 0) {
            zeroCount = 0;
            IntLen = c.length;
            for (i = 0; i < IntLen; i++) {
                n = c.substr(i, 1);
                p = IntLen - i - 1;
                q = p / 4;
                m = p % 4;
                if (n == "0") {
                    zeroCount++
                } else {
                    if (zeroCount > 0) {
                        j += o[0]
                    }
                    zeroCount = 0;
                    j += o[parseInt(n)] + l[m]
                }
                if (m == 0 && zeroCount < 4) {
                    j += k[q]
                }
            }
            j += g
        }
        if (e != "") {
            decLen = e.length;
            for (i = 0; i < decLen; i++) {
                n = e.substr(i, 1);
                if (n != "0") {
                    j += o[Number(n)] + h[i]
                }
            }
        }
        if (j == "") {
            j += o[0] + g + d
        } else {
            if (e == "") { j += d }
        }
        return j
    }
    function showPaymentFee() {
        $("#ContentPlaceHolder1_txtMoney").val($("#ContentPlaceHolder1_txtMoney").val().replace(/\D+/g, ''));
        jQuery("#chineseMoney").html(changeMoneyToChinese($("#ContentPlaceHolder1_txtMoney").val()));
    }
    function checkRecharge(){
        if(!this.amount.value) throw('请填写充值金额');
        //showPaymentFee();
        //if(isNaN(amount)) throw('充值金额错误');
        //if(!this.amount.value.match(/^\d+(\.\d{0,2})?$/)) throw('充值金额错误');
        showPaymentFee();
        var amount=parseInt(this.amount.value),
            $this=$('input[name=amount]',this),
            min=parseInt($this.attr('min')),
            max=parseInt($this.attr('max'));
        min1=parseInt($this.attr('min1')),
            max1=parseInt($this.attr('max1'));

        if($('input[name=mBankId]').val()==2||$('input[name=mBankId]').val()==3){
            if(amount<min1) throw('支付宝/微信支付充值金额最小为'+min1+'元');
            if(amount>max1) throw('支付宝/微信支付值金额最多限额为'+max1+'元');
            showPaymentFee();
        }else{
            if(amount<min) throw('充值金额最小为'+min+'元');
            if(amount>max) throw('充值金额最多限额为'+max+'元');
            showPaymentFee();

        }
        showPaymentFee();
    }
    function toCash(err, data){
        //console.log(err);
        if(err){
            alert(err)
           // $("#vcode").trigger("click");
        }else{
       /*     $(':password').val('');
            $('input[name=amount]').val('');*/
            $('.biao-cont').html(data);
        }
    }
    $(function(){
        $('input[name=amount]').keypress(function(event){
            //console.log(event);
            event.keyCode=event.keyCode || event.charCode;
            return !!(
                // 数字键
                (event.keyCode>=48 && event.keyCode<=57)
                || event.keyCode==13
                || event.keyCode==8
                || event.keyCode==9
                || event.keyCode==46
            )
        });
    });
</script>
<script type="text/javascript" src="/skin/js/swfobject.js"></script>




<style type="text/css">
    .table_b td input
    {
        height:24px;
        line-height:24px;
        padding:2px;
        border:1px #ddd solid
    }
    .table_b td input:focus
    {
        border:1px #e29898 solid;
        background-color:#ffecec
    }
</style>
<div class="biao-cont">
    <form action="/index.php/cash/inRecharge" method="post" target="ajax"  call="toCash" dataType="html">
		<?php
		$sql="select * from {$this->prename}bank_list b, {$this->prename}sysadmin_bank m where m.admin=1 and m.enable=1 and b.isDelete=0 and b.id=m.bankId";
		$banks=$this->getRows($sql);

		if($banks){?>
            <table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
                <tr class='table_b_th'>
                    <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>在线充值</td>
                </tr>

                <tr height=25 class='table_b_tr_b' >
                    <td align="center" class="copys" height="80"><div style="display:inline;color:#FF0000">自动充值须知：<div></td>
                    <td align="left" >
                        <p>每天的充值处理时间为：<b style="display:inline;color:#FF0000;">早上 &nbsp 9:00 &nbsp 至  晚上 &nbsp 21:00</b></p>
                        <p></p>
                        <p>充值后，<b style="display:inline;color:#FF0000;">请手动刷新&nbsp&nbsp</b>你的余额及查看相关账变信息</p>
                        <p>若超过5分钟未加金额，请于在线客服联系</p>
                        <p>选择充值银行，填写充值金额，点击[下一步]后，将有详细的文字说明</p>
                    </td>
                </tr>
				<?php
				$set=$this->getSystemSettings();
				foreach($banks as $bank){?>
                    <tr height=25 class='table_b_tr_b' >
                        <td align="center" class="copys">选择充值银行：</td>
                        <td align="left" >
                            <div class="bankchoice">
                                <label><input value="<?=$bank['id']?>" type="radio" name="mBankId" data-bank='<?=json_encode($bank)?>' style="width:auto;" /><span style="background:url(/<?=$bank['logo']?>);" ></span></label>
                            </div>

                            <div class="clear"></div>
                        </td>
                    </tr>
				<?php } ?>
       <!--         <tr height=25 class='table_b_tr_b'>
                    <td align="center" class="copys">充值金额：</td>
                    <td align="left" ><input name="amount" id="ContentPlaceHolder1_txtMoney" min="<?/*=$set['rechargeMin']*/?>" max="<?/*=$set['rechargeMax']*/?>" min1="<?/*=$set['rechargeMin1']*/?>" max1="<?/*=$set['rechargeMax1']*/?>" value="" onkeyup="showPaymentFee();"/><div style="display:inline;" class="spn12">(  单笔限额最低： <b style="color:#FF0000"><?/*=$set['rechargeMin']*/?></b>  元 )</div></td>
                </tr>
                <tr height=25 class='table_b_tr_b'>
                    <td align="center" class="copys">充值金额(大写)：</td>
                    <td align="left" ><strong style="color:#FF0000;margin-left:10px" id="chineseMoney"></strong></td>
                </tr>-->

                <tr height=60 class='table_b_tr_b'>
                    <td align="center" style="font-weight:bold;"></td>
                    <td id="td-btn" align="left">
                        <input type="button" id='put_button_pass' class="btn btn-danger" value="确定"  onclick="$(this).closest('form').submit()">
                     <!--   <input type="reset" value="重置" class="btn"/>--> </td>
                </tr>
            </table>
		<?php }else{ ?>
            <div style=" margin-top:30px; text-align:center;color:#F00;">
                充值暂停！
            </div>
		<?php }?>
    </form>

</div>
<!--以下为模板代码-->
<?php $this->display('inc_footer.php') ?>


